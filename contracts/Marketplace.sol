// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract MarketContract is Ownable {
    using Address for address;
    using SafeMath for uint256;

    enum State {
        INITIATED,
        SOLD,
        CANCELLED
    }

    struct Listing {
        string  listingId;
        bool    isErc721;
        State   state;
        uint256 tokenId;
        uint256 amount;
        uint256 price;
        uint256 royalty;
        address nftAddress;
        address seller;
        address erc20Address;
        address buyer;
        address royalty_recipient;
    }

    struct Fee {
        address token_address;
        uint256 amount;
    }

    mapping(address => Fee) private _fees;
    // List of all listings in the marketplace. All historical ones are here as well.
    mapping(string => Listing) private _listings;

    // 100% fee is equivalent 10% actual fee. Multply the fee e.g: 2.5 by 10 = 25 fee = 2.5% actual fee.
    uint256 private _marketplaceFee;
    address private _marketplaceFeeRecipient;
    uint256 private _marketplaceFeeDivisor;
    uint256 private _maxRoyaltyFee;

    /**
    * @dev Emitted when new listing is created by the owner of the contract. Amount is valid only for ERC-1155 tokens
    */
    event ListingCreated(bool indexed isErc721, address indexed nftAddress, uint256 indexed tokenId, string listingId, uint256 amount, uint256 price, address erc20Address, address seller);
    /**
    * @dev Emitted when listing assets were sold.
    */
    event ListingSold(address indexed buyer, string listingId);

    /**
    * @dev Emitted when listing was cancelled and assets were returned to the seller.
    */
    event ListingCancelled(string listingId);

    receive() external payable {
    }

    function onERC1155Received(address, address, uint256, uint256, bytes memory) public virtual returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(address, address, uint256[] memory, uint256[] memory, bytes memory) public virtual returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }

    function onERC721Received(address, address, uint256, bytes memory) public virtual returns (bytes4) {
        return this.onERC721Received.selector;
    }

    constructor(uint256 fee, address feeRecipient, uint256 feeDivisor, uint256 maxRoyalty) {
        _marketplaceFee = fee;
        _marketplaceFeeRecipient = feeRecipient;
        _marketplaceFeeDivisor = feeDivisor;
        _maxRoyaltyFee = maxRoyalty;
    }

    function getMarketplaceFee() public view virtual returns (uint256) {
        return _marketplaceFee;
    }

    function getMarketplaceFeeRecipient() public view virtual returns (address) {
        return _marketplaceFeeRecipient;
    }

    function getMarketplaceFeeDivisor() public view virtual returns (uint256) {
        return _marketplaceFeeDivisor;
    }

    function getMaxRoyaltyFee() public view virtual returns (uint256) {
        return _maxRoyaltyFee;
    }

    function getListing(string memory listingId) public view virtual returns (Listing memory) {
        return _listings[listingId];
    }

    function setMarketplaceFee(uint256 fee) public virtual onlyOwner {
        _marketplaceFee = fee;
    }

    function setMarketplaceFeeRecipient(address recipient) public virtual onlyOwner {
        _marketplaceFeeRecipient = recipient;
    }

    function setMarketplaceFeeDivisor(uint256 fee) public virtual onlyOwner {
        _marketplaceFeeDivisor = fee;
    }

    function setMaxRoyaltyFee(uint256 fee) public virtual onlyOwner {
        _maxRoyaltyFee = fee;
    }

    /**
    * Check if the seller is the owner of the token.
    * We expect that the owner of the tokens approves the spending before he launch the auction
    * The function escrows the tokens to sell
    **/
    function _escrowTokensToSell(bool isErc721, address nftAddress, address seller, uint256 tokenId, uint256 amount) internal {
        if (!isErc721) {
            require(amount > 0);
            require(IERC1155(nftAddress).balanceOf(seller, tokenId) >= amount, "ERC1155 token balance is not sufficient for the seller..");
            IERC1155(nftAddress).safeTransferFrom(seller, address(this), tokenId, amount, "");
        } else {
            
        }
    }

    /**
    * @dev Create new listing of the NFT token in the marketplace.
    * @param listingId - ID of the listing, must be unique
    * @param isErc721 - whether the listing is for ERC721 or ERC1155 token
    * @param nftAddress - address of the NFT token
    * @param tokenId - ID of the NFT token
    * @param price - Price for the token. It could be in wei or smallest ERC20 value, if @param erc20Address is not 0x0 address
    * @param amount - ERC1155 only, number of tokens to sold.
    * @param erc20Address - address of the ERC20 token, which will be used for the payment. If native asset is used, this should be 0x0 address
    * @param royalty - amount of royalty for sale of item
    * @param royalty_recipient - royalty fee recipient address
    */
    function createListing(string memory listingId, bool isErc721, address nftAddress, uint256 tokenId, uint256 price, address seller, uint256 amount, address erc20Address, uint256 royalty, address royalty_recipient) public {
        require(royalty <= _maxRoyaltyFee, "royalty must not exceed max royalty fee");
        if (keccak256(abi.encodePacked(_listings[listingId].listingId)) == keccak256(abi.encodePacked(listingId))) {
            revert("Listing already existed for current listing Id");
        }
        if (!isErc721) {
            require(amount > 0);
            // check if the seller owns the tokens he wants to put on auction
            // transfer the tokens to the auction house
            _escrowTokensToSell(isErc721, nftAddress, seller, tokenId, amount);
        } else {
            _escrowTokensToSell(isErc721, nftAddress, seller, tokenId, amount);
        }

        Listing memory listing = Listing(listingId, isErc721, State.INITIATED, tokenId, amount, price, royalty, nftAddress, seller, erc20Address, address(0), royalty_recipient);
        _listings[listingId] = listing;
        emit ListingCreated(isErc721, nftAddress, tokenId, listingId, amount, price, erc20Address, seller);
    }

    /**
    * @dev Buyer wants to buy NFT from listing. All the required checks must pass.
    * Buyer must either send ETH with this endpoint, or ERC20 tokens will be deducted from his account to the marketplace contract.
    * @param listingId - id of the listing to buy
    * @param erc20Address - optional address of the ERC20 token to pay for the assets, if listing is listed in ERC20
    */
    function buyAssetFromListing(string memory listingId, address erc20Address) public payable {
        require(_listings[listingId].state == State.INITIATED, "Listing is in wrong state!");
        require(_listings[listingId].erc20Address == erc20Address, "Incorrect ERC20 token address!");
        Listing memory listing = _listings[listingId];

        uint256 fee = listing.price * _marketplaceFee / _marketplaceFeeDivisor;
        uint256 royalty_fee = listing.price * listing.royalty / _marketplaceFeeDivisor; 
        uint256 seller_portion = listing.price - fee - royalty_fee;

        listing.state = State.SOLD;
        listing.buyer = _msgSender();
        _listings[listingId] = listing; // Prevent re-entrancy

        if (listing.erc20Address == address(0)) {
            if (listing.price > msg.value) {
                if (msg.value > 0) {
                    Address.sendValue(payable(_msgSender()), msg.value);
                }
                revert("Insufficient price paid for the asset.");
            }
            Address.sendValue(payable(_marketplaceFeeRecipient), fee);
            if(royalty_fee > 0) {
                Address.sendValue(payable(listing.royalty_recipient), royalty_fee);
            }
            Address.sendValue(payable(listing.seller), seller_portion);
        } else {
            IERC20 token = IERC20(listing.erc20Address);
            if (token.allowance(_msgSender(), address(this)) >= listing.price && token.balanceOf(_msgSender()) >= listing.price) {
                token.transferFrom(_msgSender(), _marketplaceFeeRecipient, fee);
                if(royalty_fee > 0) {
                    token.transferFrom(_msgSender(), listing.royalty_recipient, royalty_fee);
                }
                token.transferFrom(_msgSender(), listing.seller, seller_portion);
            }else {
                if (msg.value > 0) {
                    Address.sendValue(payable(_msgSender()), msg.value);
                }
                revert("Insufficient ERC20 allowance balance!");
            }
        }
        if (listing.isErc721) {
            if(IERC721(listing.nftAddress).ownerOf(listing.tokenId) != listing.seller) {
                revert("NFT no longer owned by seller!");
            }
            IERC721(listing.nftAddress).safeTransferFrom(listing.seller, _msgSender(), listing.tokenId);
        } else {
            if(IERC1155(listing.nftAddress).balanceOf(listing.seller, listing.tokenId) < listing.amount) {
                revert("Seller no longer has the amount of nft!");
            }
            IERC1155(listing.nftAddress).safeTransferFrom(listing.seller, _msgSender(), listing.tokenId, listing.amount, "");
        }
        emit ListingSold(_msgSender(), listingId);
    }

    /**
    * @dev Cancel listing - returns the NFT asset to the seller.
    * @param listingId - id of the listing to cancel
    */
    function cancelListing(string memory listingId) public virtual {
        Listing memory listing = _listings[listingId];
        require(listing.state == State.INITIATED, "Listing is not in INITIATED state. Aborting.");
        require(listing.seller == _msgSender() || _msgSender() == owner(), "Listing can't be cancelled from other then seller or owner. Aborting.");
        listing.state = State.CANCELLED;
        _listings[listingId] = listing;
        emit ListingCancelled(listingId);
    }

    /**
    * @dev transfer Item - transfers nft to related address.
    * @param isErc721 - returns bool if the nft is ERC721 or ERC1155
    * @param nftAddress - Address of NFT
    * @param reciever - Address of NFT reciever address
    * @param tokenId - NFT Token Id
    * @param amount - Amount of NFT to be sent
    */
    function transferItem(bool isErc721, address nftAddress, address reciever, uint256 tokenId, uint256 amount) public onlyOwner{
        if (isErc721) {
            if(IERC721(nftAddress).balanceOf(address(this)) > 0) {
                IERC721(nftAddress).safeTransferFrom(address(this), reciever, tokenId);
            }
        } else {
            if(IERC1155(nftAddress).balanceOf(address(this), tokenId) > 0) {
                IERC1155(nftAddress).safeTransferFrom(address(this), reciever, tokenId, amount, "");
            }
        }
    }

}