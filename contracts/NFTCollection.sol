// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

contract LiquidCollection is ERC721, ERC721Enumerable, ERC721URIStorage, AccessControlEnumerable {
    using Strings for uint256;

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    constructor () ERC721("NftCollection", "NFT-ERC721"){
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    struct Item {
        uint256 id;
        address creator;
        string  uri;
    }

    // Internal Base Uri
    string private _internalBaseURI;

    mapping (uint256 => Item) public Items;

    function createItem(string memory uri) public returns (uint256) {
        require(_msgSender() != address(0), "ERC1155: mint to the zero address");

        address to = _msgSender();
        // Incrementing current tokenId for unique tokenId
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _safeMint(to, newItemId);

        Items[newItemId] = Item(newItemId, to, uri);
        return newItemId;
    }

    /**
    *   @dev Fetch Item structure of the NFT
    *   Requirements:
    *   - uint256 tokenId of the NFT
    */
    function getItem(uint256 tokenId) public view returns (Item memory) {
        require(_exists(tokenId), "ERC721Metadata: Item query for nonexistent token");
        return Items[tokenId];
    }

    /**
     * @dev Base URI for computing {tokenURI}. If set, the resulting URI for each
     * token will be the concatenation of the `baseURI` and the `tokenId`. Empty
     * by default, can be overriden in child contracts.
     */
    function _baseURI() internal view override returns (string memory) {
        return _internalBaseURI;
    }

    /**
    *   @dev Set Base URI of the NFT
    *   Requirements:
    *   - string of the new BaseUri
    */
    function setBaseURI(string memory newBaseUri) public {
        require(
            hasRole(DEFAULT_ADMIN_ROLE, _msgSender()),
            "ERC721: must have admin role to change baseUri"
        );
        _internalBaseURI = newBaseUri;
    }

    /**
    *   @dev Set New Admin Role
    *   Requirements:
    *   - address of the new admin
    */
    function setUserAsAdmin(address _user) public {
        require(
            hasRole(DEFAULT_ADMIN_ROLE, _msgSender()),
            "ERC721: must have admin role to set another admin"
        );
        _setupRole(DEFAULT_ADMIN_ROLE, _user);
    }

    /**
    *   @dev Set new Token URI for the NFT
    *   Requirements:
    *   - uint256 tokenId of the NFT
    *   - string _tokenURI - new tokenURI
    */
    function setTokenURI(uint256 tokenId, string memory _tokenURI) public {
        require(_exists(tokenId), "ERC721Metadata: Item query for nonexistent token");
        require(
            hasRole(DEFAULT_ADMIN_ROLE, _msgSender()),
            "ERC721: must have admin role to set Token URIs"
        );
        _setTokenURI(tokenId, _tokenURI);
        Items[tokenId].uri = _tokenURI;
    }

    function tokenURI(uint256 tokenId) public view override(ERC721URIStorage, ERC721) returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");

        string memory baseURI = _baseURI();
        
        if(bytes(baseURI).length > 0) {
            return string(abi.encodePacked(baseURI, tokenId.toString()));
        }else {
            return Items[tokenId].uri;
        }
    }

    function burn(uint256 tokenId) public virtual {
        require(
            _isApprovedOrOwner(_msgSender(), tokenId),
            "ERC721Burnable: caller is not owner nor approved"
        );
        _burn(tokenId);
    }

    function _burn(uint256 tokenId)
        internal
        virtual
        override(ERC721, ERC721URIStorage)
    {
        super._burn(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC721Enumerable, AccessControlEnumerable, ERC721)
        returns (bool)
    {
        return
            interfaceId == type(IERC721Enumerable).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override(ERC721, ERC721Enumerable) {
        super._beforeTokenTransfer(from, to, tokenId);
    }

}